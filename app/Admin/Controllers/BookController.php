<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Book;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;
use App\Models\Type;

class BookController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new Book(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('title');
            $grid->column('author');
            $grid->column('type_id')->display(function($item) {
                return Type::find($item)->name;
            });
            $grid->column('publication_year');
            $grid->column('available');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();
        
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
        
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Book(), function (Show $show) {
            $show->field('id');
            $show->field('title');
            $show->field('author');
            $show->field('type_id');
            $show->field('publication_year');
            $show->field('available');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {


        return Form::make(new Book(), function (Form $form) {
            $categories = $this->categories();

            $form->display('id');
            $form->text('title');
            $form->text('author');        
            $form->select('type_id')->options($categories);;
            $form->text('publication_year');
            $form->text('available');
        
            $form->display('created_at');
            $form->display('updated_at');
        });
    }

    
    protected function categories()
    {
        $categories = Type::all();

        $categories = $categories->pluck('name', 'id')->toArray();

        return $categories;
    }
}
