<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable()->comment('书籍名称');
            $table->string('author')->nullable()->comment('作者');
            $table->integer('type_id')->nullable()->comment('类型');
            $table->integer('publication_year')->nullable()->comment('发布时间');
            $table->integer('available')->nullable()->comment('剩余数量');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book');
    }
}
