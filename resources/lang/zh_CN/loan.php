<?php 
return [
    'labels' => [
        'Loan' => '借阅记录',
        'loan' => '借阅记录',
    ],
    'fields' => [
        'book_id' => '书籍ID',
        'user_id' => '用户ID',
        'loan_date' => '借阅日期',
        'return_date' => '归还日期',
    ],
    'options' => [
    ],
];
