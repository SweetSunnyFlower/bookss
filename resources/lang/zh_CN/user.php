<?php 
return [
    'labels' => [
        'User' => '用户',
        'user' => '用户',
    ],
    'fields' => [
        'name' => '姓名',
        'email' => '邮箱',
        'email_verified_at' => '激活时间',
        'password' => '密码',
        'remember_token' => 'remember_token',
    ],
    'options' => [
    ],
];
