<?php 
return [
    'labels' => [
        'Book' => '书籍',
        'book' => '书籍',
    ],
    'fields' => [
        'title' => '书籍名称',
        'author' => '作者',
        'type_id' => '类型',
        'publication_year' => '发布时间',
        'available' => '剩余数量',
    ],
    'options' => [
    ],
];
